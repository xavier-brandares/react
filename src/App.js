import logo from './Assets/logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="Splash-logo" alt="logo" />
        <p>
          react site
        </p>
      </header>
    </div>
  );
}

export default App;
