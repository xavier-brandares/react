import logo from '../Assets/logo.svg';
function Splash() {
    return (
        <div className="Splash">
            <header className="Splash-header">
                <img src={logo} className="Splash-logo" alt="logo" />
                <p>
                    Splash Page
                </p>
            </header>
        </div>
    );
}

export default Splash;